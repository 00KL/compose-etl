#!/bin/bash

# Prepara network para os containers se comunicarem 
docker network create \
  --driver=bridge \
  --subnet=10.2.0.0/16 \
  --ip-range=10.2.5.0/24 \
  --gateway=10.2.5.254 \
  wize-network

# Cria docker contendo o banco de dados (chamado sro_ontology) 
# onde os dados da empresa serão armazenados
# seguindo a SRO ontology.
# As credênciais do banco são: user=postgres
#                              senha=1234
docker run --name postgres --network=wize-network --ip 10.2.0.3 \
-p 5432:5432 \
-e "POSTGRES_USER=postgres" \
-e "POSTGRES_PASSWORD=1234" \
-e "POSTGRES_DB=sro_ontology" \
-d postgres 

# Monta as tabelas no banco Sro_ontology seguindo 
# a SRO ontology
docker run --name create-db --network=wize-network \
  -e "USERDB=postgres" \
  -e "PASSWORDDB=1234" \
  -e "DBNAME=sro_ontology" \
  -e "HOST=10.2.0.3" \
  -e "DBMETABASE=metabase" \
  00kl/create_db-wize 

# Cria um docker com um banco Mongo para servir 
# de log de criação e atualização dos dados 
# gravados no banco sro_ontology.
# Também serve como etapa intermediária para 
# o preenchimento do banco, almentando a velocidade
# do preenchimento do banco.
docker run --name mongodb --network=wize-network \ 
  --ip 10.2.0.6 \
  -p 27017:27017 \
  -d mongo:4.4.6-bionic \

# Cria docker com RabbitMQ para servir como
# fila de pacotes.
docker run --name rabbit --network=wize-network \
  --ip 10.2.0.2 \
  -p 15672:15672 \
  -p 5672:5672 \ 
  -d rabbitmq:3-management 

# Posta pacotes na fila do RabbitMQ
docker run --name realtime --network=wize-network \
  -p 8090:8090 \
  -e "CLOUDAMQP_URL=10.2.0.2" \
  -d 00kl/realtime-wize:latest 

# Cria container para a interface de consulta
# e análise dos dados coletados
docker run --name metabase --network=wize-network \
  -p 3000:3000 \
  -d 00kl/metabase-wize

# Cria container para consumir pacotes da fila (RabbitMQ)
# para converter os dados vindos do Jira para o formato
# aceito pela SRO ontology e então salvar os dados 
# no banco sro_ontology
docker run --name jira_hub --network=wize-network \
  -e "RABBITMQ_HOST=10.2.0.2" \
  -e "HOST=10.2.0.3" \
  -e "MongoDB=10.2.0.6" \
  -e "DBNAME=sro_ontology" \
  -e "USERDB=postgres" \
  -e "PASSWORDDB=1234" \
  00kl/jira_hub-wize:latest 


# Extra: Pgadmin - ferramenta de administração de bancos de dados
# Não á necessário para rodar os seriviços acima listados
# docker run --name pgadmin --network=myNetwork \
#   -p 15432:80 \
#   -e "PGADMIN_DEFAULT_EMAIL=admin@admin.com.br" \
#   -e "PGADMIN_DEFAULT_PASSWORD=admin" \
#   -d dpage/pgadmin4
